<?php
	if (@$_REQUEST['delete']=="yes") {
		$message_id = $_REQUEST['messageid'];
		
		$xml = simplexml_load_file('database.xml');
		
		foreach ($xml->message as $message) {
			if ($message['id'] == $message_id) {
				if ($message->session == $session_id) {
					$dom = dom_import_simplexml($message);
					$dom->parentNode->removeChild($dom);
				}
			}
		}

		$xml->asXML('database.xml');
		
		header('Location: feed.php');
		exit;
	}
?>