<?php
	$mssg = $_POST['mssg'];
	$xml = simplexml_load_file('database.xml');

	if ($xml->message[count($xml)] > 0) {
		$last_id = $xml->message[count($xml)-1]['id'];
	}
	else $last_id = -1;

	$newNode = $xml->addChild('message');
	$newNode->addAttribute('id',$last_id+1);

	$newNode->addChild('date',date("Y-m-d h:i:sa"));
	$newNode->addChild('text',$mssg);
	$newNode->addChild('session',$session_id);

	$xml->asXML('database.xml');
?>