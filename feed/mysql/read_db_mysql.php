<?php
	include('create_connection.php');
	
	$sql = "SELECT id, reg_date, text, session FROM messages ORDER BY reg_date DESC";
	$result = $mysqli->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row
		while($message = $result->fetch_assoc()) {
			if ($message["session"] == $session_id) {
				echo "<p class=\"fd_date\">" . $message["reg_date"] . "</p><p class=\"fd_text\">" . $message["text"] . "</br><a href=\"feed.php?delete=yes&messageid=" . $message["id"] . "\">Delete</a></p>";
			}
			else {
				echo "<p class=\"fd_date\">" . $message["reg_date"] . "</p><p class=\"fd_text\">" . $message["text"] . "</p>";
			}
		}
	}
	else {
		echo "<p class=\"fd_text\">They say nothing.</p>";
	}
	$mysqli->close();
?>