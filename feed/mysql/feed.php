<?php
	include('session.php');
	include('global_variables.php');
	include('check_database_mysql.php');
	include('delete_message_mysql.php');
?>

<!DOCTYPE html>

<HTML lang="en-GB">
	<head>
		<meta charset="utf-8" name="viewport" content="width=device-width">
		<title>Feed</title>
		<link rel="stylesheet" href="stl.css">
	</head>
	<body>
		<h1>Welcome</h1>
		<div id="border">
			<div id="feed">
				<p>
				It's a simple page intended for collecting short messages. You may delete your post for a short duration of time, so hurry up!
				</p>
				<h2>Please enter your message below</h2>
				<form method="post" id="message_form">
					<textarea maxlength="255" rows="5" name="mssg" id="message_field" title="Your text here"></textarea>
					<input type="submit" name="button" id="message_submit" value="Send!"/>
				</form>
				<h2>What others say...</h2>
				<?php
					if ($_POST) {
						if ($_POST['mssg']) {
							include('add_to_db_mysql.php');
						}
					}
					include('read_db_mysql.php');
				?>
			</div>
		</div>
	</body>
</HTML>