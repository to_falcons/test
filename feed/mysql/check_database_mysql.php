<?php
	// create connection
	$mysqli = new mysqli($servername, $username, $password);

	// check connection
	if ($mysqli->connect_error) {
	  die("Connection failed: " . $mysqli->connect_error);
	}

	$sql = 'SELECT COUNT(*) AS `exists` FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMATA.SCHEMA_NAME="feed_database"';

	// execute the statement
	$query = $mysqli->query($sql);

	// extract the value
	$row = $query->fetch_object();
	$dbExists = (bool) $row->exists;

	// if database doesn't exist
	if ($dbExists == NULL) {
		$sql = "CREATE DATABASE feed_database";
		if ($mysqli->query($sql) === TRUE) {
			$mysqli->close();
			
			$mysqli = new mysqli($servername, $username, $password, $dbname);
			
			if ($mysqli->connect_error) {
				die("Connection failed: " . $mysqli->connect_error);
			}
			
			$sql = "CREATE TABLE messages (
				id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				text VARCHAR(255) NOT NULL,
				session VARCHAR(32),
				reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
			)";

			if ($mysqli->query($sql) === FALSE) {
				echo "Error creating table: " . $mysqli->error;
			}
		} else {
			echo "Error creating database: " . $mysqli->error;
		}
	}

	$mysqli->close();
?>
