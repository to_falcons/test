<?php
	// create connection
	$mysqli = new mysqli($servername, $username, $password, $dbname);

	// check connection
	if ($mysqli->connect_error) {
		die("Connection failed: " . $mysqli->connect_error);
	}
?>